{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -w #-}
module Paths_10001st_prime (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where


import qualified Control.Exception as Exception
import qualified Data.List as List
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude


#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir `joinFileName` name)

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath



bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath
bindir     = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\bin"
libdir     = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\lib\\x86_64-windows-ghc-9.2.5\\10001st-prime-0.1.0.0-nntJCjgLlpEiKR6PthYjo-10001st-prime"
dynlibdir  = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\lib\\x86_64-windows-ghc-9.2.5"
datadir    = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\share\\x86_64-windows-ghc-9.2.5\\10001st-prime-0.1.0.0"
libexecdir = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\libexec\\x86_64-windows-ghc-9.2.5\\10001st-prime-0.1.0.0"
sysconfdir = "E:\\prog\\haskell\\10001st-prime\\.stack-work\\install\\5d962c63\\etc"

getBinDir     = catchIO (getEnv "10001st_prime_bindir")     (\_ -> return bindir)
getLibDir     = catchIO (getEnv "10001st_prime_libdir")     (\_ -> return libdir)
getDynLibDir  = catchIO (getEnv "10001st_prime_dynlibdir")  (\_ -> return dynlibdir)
getDataDir    = catchIO (getEnv "10001st_prime_datadir")    (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "10001st_prime_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "10001st_prime_sysconfdir") (\_ -> return sysconfdir)




joinFileName :: String -> String -> FilePath
joinFileName ""  fname = fname
joinFileName "." fname = fname
joinFileName dir ""    = dir
joinFileName dir fname
  | isPathSeparator (List.last dir) = dir ++ fname
  | otherwise                       = dir ++ pathSeparator : fname

pathSeparator :: Char
pathSeparator = '\\'

isPathSeparator :: Char -> Bool
isPathSeparator c = c == '/' || c == '\\'
