module Main (main) where

primes :: [Int]
primes = 2 : filterMult allMult [3,5..]
  where
    allMult = mergeMult $ map mult primes
    mult i = map (i*) [i..]

filterMult :: Ord a => [a] -> [a] -> [a]
filterMult (f:fs) (x:xs)
  | f<x = filterMult fs (x:xs)
  | f>x = x : filterMult (f:fs) xs
  | otherwise = filterMult fs xs

merge :: Ord a => [a] -> [a] -> [a]
merge (x:xs) (y:ys)
  | x<y = x : merge xs (y:ys)
  | x>y = y:merge (x:xs) ys
  | otherwise = x : merge xs ys

mergeMult :: Ord a => [[a]] -> [a]
mergeMult ((x:xs):xss) = x : merge xs (mergeMult xss)

main :: IO ()
main = do
  print $ primes !! 10000
