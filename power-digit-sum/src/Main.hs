module Main (main) where
import Data.Char
import Data.Function

digSum :: Int
digSum = sum $ map digitToInt (show (2^1000))

main :: IO ()
main = do
  print digSum
