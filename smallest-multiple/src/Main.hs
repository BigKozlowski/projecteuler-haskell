module Main (main) where

main :: IO ()
main = do
  print $ foldl lcm 1 [2..20]
