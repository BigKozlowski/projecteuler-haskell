module Main (main) where
import Data.Char
import Data.List
import System.Directory

main :: IO ()
main = do
  cwd <- getCurrentDirectory
  let path = cwd ++ "/src/num.txt"
  str <- readFile path
  print . maximum . map product
        . foldr (zipWith (:)) (repeat [])
        . take 13 . tails . map (fromIntegral . digitToInt)
        . concat . lines $ str
