module Main (main) where

largestPrimeFactor :: Int -> Int
largestPrimeFactor n
  | n <= 1 = error "illegal input"
  | otherwise = getFactor n (2: [3, 5..])
                where
                  getFactor n odds@(c:cs)
                    | c*c >= n = n
                    | m == 0 = getFactor d odds
                    | otherwise = getFactor n cs
                      where
                        (d, m) = divMod n c

main :: IO ()
main = do
  print $ largestPrimeFactor 600851475143
