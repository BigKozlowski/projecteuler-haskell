module Main (main) where

sqr :: Int -> Int
sqr n = n*n

sumSquare :: Int -> Int
sumSquare n = foldl (+) 0 $ map sqr [1..n]

squareSum :: Int -> Int
squareSum n = sqr $ foldl (+) 0 [1..n]

sqrDiff :: Int -> Int
sqrDiff n = (squareSum n) - (sumSquare n)

main :: IO ()
main = do
  print $ sqrDiff 100
