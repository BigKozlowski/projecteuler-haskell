module Main (main) where

isPal :: Show a => a -> Bool
isPal x = x' == reverse x' where x' = show x

largestPal :: Int
largestPal = maximum [x*y | x <- [999,998..100], y <- [999, 998..100]
                     , x >= y
                     , isPal (x*y)]

main :: IO ()
main = do
  print largestPal
