module Main (main) where

isMultipleOf :: Int -> Bool
isMultipleOf n
  | n `rem` 5 == 0 = True
  | n `rem` 3 == 0 = True
  | otherwise = False

getMultiplesSum :: Int -> Int
getMultiplesSum n =
  foldl (+) 0 $ filter isMultipleOf [0..n-1]

main :: IO ()
main = do
  print $ getMultiplesSum 1000
