module Main (main) where

fib :: Int -> Int
fib = (map fibm [0..] !!)
  where fibm 1 = 1 
        fibm 2 = 2
        fibm n = fib (n-1) + fib (n-2)

fibs :: Int -> [Int]
fibs limit =
  getList limit [] 1
  where
    getList limit xs n
      | fib n > limit = reverse xs
      | otherwise = getList limit (fib n:xs) (n+1)



main :: IO ()
main = do
  print $ foldl (+) 0 $ filter even $ fibs 4000000
